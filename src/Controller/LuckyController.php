<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LuckyController extends AbstractController
{
    const palabras = array(
        "marcas",
        "programación",
        "entornos",
        "sistemas",
        "orientación",
        "cliente",
        "servidor",
        "diseño",
        "despliegue",
        "empresa"
    );

    const materias = array(
        "marcas" => "Linguaxe de marcas",
        "programación" => "Programación",
        "entornos" => "Entornos de desarrollo",
        "sistemas" => "Sistemas informáticos",
        "orientación" => "Formación e orientación laboral",
        "cliente" => "Desenvolvemento de aplicacións en entorno cliente",
        "servidor" => "Desenvolvemento de aplicacións en entorno servidor",
        "diseño" => "Deseño de interfaces web",
        "despliegue" => "Despliegue de aplicacións web",
        "empresa" => "Empresa e iniciativa emprendedora"
    );

    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('index.html.twig',[]);
    }

    #[Route('/lucky/number', name: 'lucky_number')]
    public function number(): Response
    {
        $number = random_int(0, count( LuckyController::palabras));

        return $this->render('lucky/number.html.twig', [
            'my_number' => LuckyController::palabras[$number],
        ]);

    }
    #[Route('/materias', name: 'materias')]
    public function vista_materias(): Response
    {
        $indice_materia = random_int(0, count(LuckyController::materias)-1);
        $claves = array_keys(LuckyController::materias);
        $materia = $claves[$indice_materia];
        $desc_materia = LuckyController::materias[$materia];

        return $this->render('materias.html.twig', [
            'materia' => $materia,
            'descricion' => $desc_materia,
        ]);
    }

    #[Route('/materias/{nome}', name: 'materias_id')]
    public function materias_id(string $nome) : Response
    {
        $desc_materia = LuckyController::materias[$nome];

        return $this->render('materias.html.twig', [
            'materia' => $nome,
            'descricion' => $desc_materia,
        ]);
    }
}